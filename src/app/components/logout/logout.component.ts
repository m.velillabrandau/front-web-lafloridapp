import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: []
})
export class LogoutComponent implements OnInit {
  public sessionEnd: string;
  constructor(private routes: Router, private activatedRoute: ActivatedRoute) {
    this.sessionEnd = this.activatedRoute.snapshot.paramMap.get('end');
  }

  ngOnInit() {
    sessionStorage.clear();
    if (this.sessionEnd == null) {
      this.sessionEnd = '';
    }
    
    this.routes.navigate(['/login/' + this.sessionEnd]);
  }

}
