import { Component, OnInit } from '@angular/core';
import { IncidenceService } from '../../services/incidence.service';

declare var google;


@Component({
  selector: 'app-incident-map',
  templateUrl: './incident-map.component.html',
  styleUrls: ['./incident-map.component.css']
})
export class IncidentMapComponent implements OnInit {

  urlIcon = "/icons/marker-blue.png";
  groupedKmeansIncidents;
  urlKml = "./test-kml.kml"

  latitude = -33.5285700;
  longitude = -70.5395700;

  myLatLng = {
    lat: this.latitude,
    lng: this.longitude
  };

  incidents;

  showKmean: boolean;

  filters = [];

  constructor(private incidenceService: IncidenceService) { }

  ngOnInit() {

    this.showKmean = false;
    this.getKmeansIncidents();
    this.getAllIncidentsFilter(this.filters);
    // this.getAllIncidents();
  }

  async loadMap(){

    // const loading = await this.loadCtrl.create();
    // loading.present();

    // const rta = await this.geolocation.getCurrentPosition();
    // const myLatLng = {
    //   lat: rta.coords.latitude,
    //   lng: rta.coords.longitude
    // }
    
    console.log(this.myLatLng);
    const mapEle: HTMLElement = document.getElementById('map');
    const map = new google.maps.Map(mapEle,{
      center: this.myLatLng,
      zoom: 12,
      streetViewControl: false,
      disableDefaultUI: true
    });
    google.maps.event.addListenerOnce(map, 'idle', () =>{
      // loading.dismiss();
      const marker = new google.maps.Marker({
        position: {
          lat: this.myLatLng.lat,
          lng: this.myLatLng.lng
        },
        map: map,
        title: '¡ Estoy aca !'
      })
    });
  }

  placeMarker($event){

    console.log("LAT", $event.coords.lat);
    console.log("LNG", $event.coords.lng);

    let data = {
      name: 'Test',
      tag: 'Transito', 
      lat: $event.coords.lat, 
      lng: $event.coords.lng
    }

    this.incidenceService.addIncidence(data).then(res => {
      console.log("¡ Punto agregado ! ",res);
    }).catch(err=> {
      //err
    });


  }

  getAllIncidents(){
    this.incidenceService.getAllIncidents().then(res => {
      console.log("¡ Incidencias ! ", res);
      this.incidents = res;
    }).catch(err=> {
      //err
    });
  }

  getAllIncidentsFilter(filters){
    this.incidenceService.getAllIncidentsFilter(filters).then(res => {
      console.log("¡ Incidencias Filtradas! ", res);
      this.incidents = res;
    }).catch(err=> {
      //err
    });
  }

  getKmeansIncidents(){
    this.incidenceService.getKmeansIncidents().then(res => {
      console.log("¡ Incidencias Agrupadas Segun Kmeans ! ", res);
      this.groupedKmeansIncidents = res;
    }).catch(err=> {
      //err
    });
  }
  
  onChangeCover(event) {

    if(event.checked){
      // console.log("CHECK: ", event.source.value);
      this.filters.push(event.source.value);
      // console.log("Filtros: ", this.filters);
    }else{
      // console.log("CHECK: ", event.source.value);
      const index: number = this.filters.indexOf(event.source.value);
      if (index !== -1) {
          this.filters.splice(index, 1);
      }

    }

    console.log("Filtros: ", this.filters);
    
  }

  filtersSubmit(){
    this.getAllIncidentsFilter(this.filters);
  }

  kmeansGroup(){
    if(this.showKmean == false){
      this.showKmean = true;
    }else{
      this.showKmean = false;
    }
  }

}
