import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { CatalogueService } from '../../../services/catalogue.service';
import { FeriaService } from '../../../services/feria.service';

export interface Ferias {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-catalog',
  templateUrl: './add-catalog.component.html',
  styleUrls: ['./add-catalog.component.css']
})

export class AddCatalogComponent implements OnInit {
  catalogAddForm: FormGroup;
  ferias: Ferias[] = [];
  constructor(public dialogRef: MatDialog, private catService: CatalogueService, private feriaService: FeriaService) { }

  // Iniciación de clase
  ngOnInit() {
    this.getferias();
    this.catalogAddForm = new FormGroup({
      'title': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'feria': new FormControl(null,[
        Validators.required,
      ]),
    });
   }

  // Cierra popup de ingreso
  onNoClick(): void {
    this.dialogRef.closeAll();
  }
  //Carga las ferias en la interface
  getferias() {
    this.feriaService.getFerias().subscribe((data: {}) => {
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          this.ferias.push({value: key, viewValue: data[key]})
        }
      }
    });
  }
  // Reinicio componentes
  onSubmit() {
    if (this.catalogAddForm.valid) {
      this.catService.addCatalog({"feria_alias":this.title.value, 'ferias_id': this.feria.value});
      this.dialogRef.closeAll();
      this.dialogRef.closeAll();
    }
  }

  get title() { return this.catalogAddForm.get('title'); }
  get feria() { return this.catalogAddForm.get('feria'); }
  matcher = new ErrorStateMatcher();
}
