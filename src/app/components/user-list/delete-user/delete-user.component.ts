import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from "../../shared/dialog/dialog.component";
import { UserService } from '../../../services/user.service';
import { SwalComponent } from '../../shared/swal/swal.component';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {
  data: Array<any> = [];
  constructor(public dialogRef: MatDialog, public dialog: DialogComponent, public userService: UserService) { }

  ngOnInit() {
    this.data = this.dialog.getData();
  }
  onNoClick(): void {
    this.dialogRef.closeAll();
  }
  deleteUser() {
    this.userService.deleteUser(this.data["id"]);
    this.dialogRef.closeAll();
  }
}
