import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../shared/dialog/dialog.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UserService } from '../../services/user.service';
import { EditUserComponent } from './edit-user/edit-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { DataSource } from '@angular/cdk/table';

export interface UserData {
	created: Date;
	username: string;
}
@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['username', 'created', 'acciones'];
	dataSource: MatTableDataSource<UserData>;
	@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

	constructor(public dialog: MatDialog, public userService: UserService) { }

	ngOnInit() {
    this.getUsers();
    this.userService.change.subscribe(change => {
      this.getUsers();
    });
  }
  getUsers(){
    this.userService.getUsers().subscribe((data: {}) => {
      this.dataSource = new MatTableDataSource(data as Array<UserData>);
      //Configuraciones paginador y ordenado
      this.paginator._intl.itemsPerPageLabel = 'Registros por página';
      this.paginator._intl.nextPageLabel = 'Página siguiente';
      this.paginator._intl.previousPageLabel = 'Página anterior';
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
	//Filtros
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
  }

  openModalUser(): void{
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: {component: AddUserComponent, title: "Crear usuario nuevo"}
    });
  }
  openModalEditUser(id, username): void{
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '340px',
      data: { component: EditUserComponent, title: "Editar Usuario", id: id, username: username }
    });
  }
  openModalDeleteUser(id): void{
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '340px',
      data: { component: DeleteUserComponent, title: "Eliminar Usuario", id: id }
    });
  }
}
