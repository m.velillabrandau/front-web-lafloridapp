import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core'
import { SwalComponent } from '../shared/swal/swal.component';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  userForm: FormGroup;
  public sessionEnd: string;
  constructor(private router: Router, private loginService: LoginService, private activatedRoute: ActivatedRoute) {
    this.sessionEnd = this.activatedRoute.snapshot.paramMap.get('end');

    document.body.className = "backgroundimg";
  }

  hide: 1;

  ngOnInit() {
    // Parametros de validación de campos
    this.userForm = new FormGroup({
      'user': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'pass': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
    })

    if (this.sessionEnd == '1') {
      SwalComponent.toast({
        type: 'error',
        title: 'No se ha podido autenticar al usuario, intente nuevamente.'
      });
    }
  }

  onFormSubmit() {
    // Validación de usuario ingresado
    if(this.userForm.valid) {

      let promise = this.loginService.loginUser(this.user.value, this.pass.value)
        .then(res => {
          this.router.navigate(['/catalogue-list']);
        }).catch(error => {
        });
    }
  }
  get user() { return this.userForm.get('user'); }
  get pass() { return this.userForm.get('pass'); }
  matcher = new ErrorStateMatcher();
}
