export class Userdata {
    id: number;
    token: string;
    creadaEn: Date;
    terminadaEn: Date;
    terminada: boolean;

    constructor(token: string) {
        this.token = token;
        this.creadaEn = new Date();
        this.terminadaEn = new Date();
        this.terminada = false;
        this.id = new Date().getTime();
    }
}