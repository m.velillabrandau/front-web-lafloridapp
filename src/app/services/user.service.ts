import { Injectable, Output, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Globals } from '../globals'
import { environment } from './../../environments/environment';
import { SwalComponent } from './../components/shared/swal/swal.component';
const APIEndpoint = environment.APIEndpoint; //rute api
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {
  @Output() change: EventEmitter<boolean> = new EventEmitter(); // Evento cambio mundo para los ga

  constructor(private http: HttpClient, private globals: Globals) { }
  //Obtiene los usuarios
  public getUsers(): Observable<any> {
    return this.http.get(APIEndpoint + '/users');
  }
  //Obtiene los usuarios
  public getUser(user): Observable<any> {
    return this.http.get(APIEndpoint + '/users/'+user);
  }

  // Elimina usuarios
  public deleteUser (id) {
    let promise = new Promise((resolve, reject) => {
      this.http.delete<any>(APIEndpoint + '/users/' + id).toPromise().then(res=> {
        this.change.emit(true);
        SwalComponent.toast({
          type: 'success',
          title: 'Usuario eliminado'
        });
      },
      msg => {
        console.log(msg); //error ocurrido
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido eliminar el usuario, por favor intente nuevamente'
        });
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }
  // Agrega usuarios
  public addUser (user){
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/users', user, httpOptions).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Usuario guardado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido guardar el usuario, por favor intente nuevamente'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }
  // Editar usuarios
  public editUser (user){
    console.log(user);
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/users/'+user.id, user, httpOptions).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Usuario editado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido editar el usuario, por favor intente nuevamente'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }
}
