import { TestBed } from '@angular/core/testing';

import { IncidenceService } from './incidence.service';

describe('IncidenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncidenceService = TestBed.get(IncidenceService);
    expect(service).toBeTruthy();
  });
});
