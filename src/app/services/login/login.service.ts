import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SwalComponent } from './../../components/shared/swal/swal.component';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from './../../../environments/environment';

interface response {
  status: boolean,
  message: string
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

const jwtHelper = new JwtHelperService();
const APIEndpoint = environment.APIEndpoint; //rute api

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Function to login through the api service
   * @author saraneda
   */
  loginUser(username: string, password: string) {

    let promise = new Promise((resolve, reject) => {

      let userLoginData = {
        username: username,
        password: password
      }

      this.http.post<any>(APIEndpoint + '/users/token', userLoginData, httpOptions).toPromise().then(res => {
        this.setSession(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Ingreso al sistema.'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log('55:login.service.ts');
          console.log(msg);
          sessionStorage.clear(); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido autenticar al usuario en este momento, intente nuevamente.'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }

  /**
   * Function to save in datastorage token related information
   * @param authResult returned data from api login
   * @author saraneda
   */
  private setSession(authResult:any) {
    sessionStorage.setItem('id_token', authResult.data.token);
    sessionStorage.setItem("expires_at", JSON.stringify(jwtHelper.getTokenExpirationDate(authResult.data.token)));
  }

  /**
   * Logout from system and remove stored data
   * @author saraneda
   */
  logout() {
    sessionStorage.clear();
  }

  /**
   * Function to check if token is expired
   * @author saraneda
   */
  get isLoggedIn() {
    return !jwtHelper.isTokenExpired(sessionStorage.getItem('id_token'));
  }

}
