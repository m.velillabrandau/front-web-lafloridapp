import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
const APIEndpoint = environment.APIEndpoint; //rute api
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})

export class FeriaService {

  constructor(private http: HttpClient) { }
  public getFerias(): Observable<any> {
    return this.http.get(APIEndpoint + '/ferias');
  }
}
