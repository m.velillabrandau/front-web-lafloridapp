import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

const httpOptions2 = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class IncidenceService {

  constructor(private http: HttpClient) { }

  getAllIncidents(){
    let promise = new Promise((resolve, reject) => {
      this.http.get(environment.APIEndpoint+ '/incidents', httpOptions2).toPromise().then(res=> {
        console.log("HOLAAAAAAAAAAAAAAAA",res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }

  getAllIncidentsFilter(filters){

    let promise = new Promise((resolve, reject) => {
      this.http.post(environment.APIEndpoint+'/incidents/indexfilter', filters ,httpOptions2).toPromise().then(res=> {
        console.log("INDEX FILTER",res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }

  getKmeansIncidents(){
    let promise = new Promise((resolve, reject) => {
      this.http.get(environment.APIEndpoint+'/incidents/kmeans', httpOptions2).toPromise().then(res=> {
        console.log("Respuesta Incidents Kmeans",res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }

  addIncidence(data){
    console.log("DATA: ", data);
    let promise = new Promise((resolve, reject) => {
      this.http.post(environment.APIEndpoint + '/incidents', data, httpOptions2).toPromise().then(res=> {
        //console.log(res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }


}
