import { Injectable, EventEmitter, Output  } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
// Servicio para mediar entre la llamada al nabvar de productos y los GA
export class MediatorService {
  sideNav: any;

  sideNavUpdated  = new EventEmitter();
  @Output() change: EventEmitter<number> = new EventEmitter();
  // Dimension Y en la grilla
  @Output() setArrY: EventEmitter<Array<any>> = new EventEmitter();
  // Dimensión X en la grilla
  @Output() setArrX: EventEmitter<Array<any>> = new EventEmitter();
  // Id Page
  @Output() setIdPage: EventEmitter<number> = new EventEmitter();
  // La Dimensión de la grilla ha cambiado
  @Output() changeGrid: EventEmitter<number> = new EventEmitter();
  // Id de mundo local de salida
  @Output() setIdWorldLocal: EventEmitter<number> = new EventEmitter();
  // Emite true para reordenar los mundos
  @Output() changueOrderWorld: EventEmitter<boolean> = new EventEmitter();
  // Emite true para reordenar los ga
  @Output() changueOrderGa: EventEmitter<boolean> = new EventEmitter();
  // Emite true para recargar grilla
  @Output() reloadGridE: EventEmitter<number> = new EventEmitter();
  @Output() changePageEvent: EventEmitter<number> = new EventEmitter();
  @Output() changeWorldNameLocal: EventEmitter<any> = new EventEmitter();

  // Emite la estructura de una página
  @Output() StructureEvent: EventEmitter<Array<any>> = new EventEmitter();

  // Emite la estructura de una página
  @Output() getIdPage: EventEmitter<number> = new EventEmitter();

  //Agrega productos al listado
  @Output() addProduct: EventEmitter<Array<any>> = new EventEmitter();

  // Emite true para mostrar los ga
  @Output() showGa: EventEmitter<boolean> = new EventEmitter();

  //EVENTO EJ
  @Output() changeAllCheckGa: EventEmitter<number> = new EventEmitter();

  constructor() {}
  getSideNavState() {
    return this.sideNavUpdated;
  }

  public setSideNavState (state, idGa, products) {
    this.sideNav = state;
    this.sideNavUpdated.emit(this.sideNav);
    this.change.emit(products);
  }
  // Envía las Y, cantidad de filas de la grilla
  public sendYcoord(yrows) {
    this.setArrY.emit(yrows);
  }
  // Envía las X, cantidad de columnas de la grilla
  public sendXcoord(Xrows) {
    this.setArrX.emit(Xrows);
  }
  //Envía el ID de una página
  public sendIdPage(idPage) {
    this.setIdPage.emit(idPage);
  }
   //Envía el ID de una página
   public sendChangueGrid(id) {
    this.changeGrid.emit(id);
  }
  //Envía el id del mundo local
  public sendIdWorldLocal(id) {
    this.setIdWorldLocal.emit(id);
  }
  //Envía el id del mundo local
  public sendWorldNameLocal(name:any) {
    this.changeWorldNameLocal.emit(name);
  }
  public reorderWorlds() {
    this.changueOrderWorld.emit(true);
  }
  public reorderGa() {
    this.changueOrderGa.emit(true);
  }
  public reloadGrid(idPage) {
    this.reloadGridE.emit(idPage);
  }
  public changePage(idPage) {
    this.changePageEvent.emit(idPage);
  }
  public sendStructure(structure) {
    this.StructureEvent.emit(structure);
  }
  public sendProductToList(product) {
    this.addProduct.emit(product);
  }
  public sendIdPageFooter(idPage) {
    this.getIdPage.emit(idPage);
  }

  public loadGa(aux){
    this.showGa.emit(aux);
  }

  public emitChangeCheckedGa(isChecked){
    this.changeAllCheckGa.emit(isChecked);
  }
}
