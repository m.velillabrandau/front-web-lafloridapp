import { Injectable, Output, EventEmitter} from '@angular/core';
import { SwalComponent } from './../components/shared/swal/swal.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Globals } from '../globals'
import { environment } from './../../environments/environment';


const APIEndpoint = environment.APIEndpoint; //rute api

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  @Output() change: EventEmitter<boolean> = new EventEmitter(); // Evento cambio mundo para los ga
  constructor(private http: HttpClient, private globals: Globals) { }

  // Obtiene el contenido de un catalogo a través de un id de feria
  public getContentMagazine($id): Observable<any> {
    return this.http.get(APIEndpoint + '/magazines/magazinecontent/'+$id);
  }
  // Obtiene todos los catálogos
  public getCatalogues(): Observable<any> {
    return this.http.get(APIEndpoint + '/magazines');
  }
  public getCataloge(id): Observable<any> {
    return this.http.get(APIEndpoint + '/magazines/' + id);
  }
  // Agrega catálalgos
  public addCatalog (catalog) {
    let promise = new Promise((resolve, reject) => {
      let body = {
        feria_alias:catalog.feria_alias,
        ferias_id:catalog.ferias_id
      }
      this.http.post<any>(APIEndpoint + '/magazines', body, httpOptions ).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Catálogo creado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido guardar el Catálogo, por favor intente nuevamente'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }

  // Editar catálalgos
  public editCatalog (updateData) {
    let promise = new Promise((resolve, reject) => {
      let formData: FormData = new FormData();
      formData.append('updateData', updateData);
      this.http.put<any>(APIEndpoint + '/magazines/'+updateData.id, updateData, httpOptions).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Catálogo editado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido editar el catálogo, por favor intente nuevamente'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }

  // Elimina catálogos
  public deleteCatalog (id) {
    let promise = new Promise((resolve, reject) => {
      this.http.delete<any>(APIEndpoint + '/magazines/' + id).toPromise().then(res=> {
        this.change.emit(true);
        SwalComponent.toast({
          type: 'success',
          title: 'Catálogo eliminado'
        });
      },
      msg => {
        console.log(msg); //error ocurrido
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido eliminar el catálogo, por favor intente nuevamente'
        });
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }
}
