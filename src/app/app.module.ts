import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//Material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatCardModule,MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatSelectModule, MatCheckboxModule, MatProgressBarModule, MatTooltipModule, MatSnackBarModule } from '@angular/material';
import { NavComponent } from './nav/nav.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import {PortalModule} from '@angular/cdk/portal';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

//Flex layout
import { FlexLayoutModule } from '@angular/flex-layout';
//Sweet alert
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
//Páginas
import { UserListComponent } from './components/user-list/user-list.component';
import { CatalogueListComponent } from './components/catalogue-list/catalogue-list.component';
import { FooterComponent } from './footer/footer.component';
import { AddUserComponent } from './components/user-list/add-user/add-user.component';
import { LoginComponent } from './components/login/login.component';
import { DialogComponent } from './components/shared/dialog/dialog.component';
import { AddCatalogComponent } from './components/catalogue-list/add-catalog/add-catalog.component';
import { SwalComponent } from './components/shared/swal/swal.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ServiceNotFoundComponent } from './service-not-found/service-not-found.component';
// Dragula
import { DragulaModule } from 'ng2-dragula';
import { HttpClientModule } from '@angular/common/http';
import { DeleteCatalogueComponent } from './components/catalogue-list/delete-catalogue/delete-catalogue.component';

import {Globals} from './globals'
import { DeleteUserComponent } from './components/user-list/delete-user/delete-user.component';
// import { EditCatalogueComponent } from './components/catalogue-list/edit-catalogue/edit-catalogue.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptorService } from './services/Loader-interceptor.service';
import { ProgressBarComponent } from './nav/progress-bar/progress-bar.component';
import { EditCatalogComponent } from './components/catalogue-list/edit-catalog/edit-catalog.component';

import { LogoutComponent } from './components/logout/logout.component';

import { EditUserComponent } from './components/user-list/edit-user/edit-user.component';
import { SkumodPipe } from './pipes/skumod.pipe';
import { NoblankDirective } from './directives/noblank.directive';
import { IncidentMapComponent } from './components/incident-map/incident-map.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
	declarations: [
		AppComponent,
		NavComponent,
		UserListComponent,
		CatalogueListComponent,
		FooterComponent,
    AddUserComponent,
    LoginComponent,
    DialogComponent,
    AddCatalogComponent,
    SwalComponent,
    PageNotFoundComponent,
    ServiceNotFoundComponent,
    DeleteCatalogueComponent,
    DeleteUserComponent,
    ProgressBarComponent,
    LogoutComponent,
    EditCatalogComponent,
    EditUserComponent,
    SkumodPipe,
    NoblankDirective,
    IncidentMapComponent,
  ],
  entryComponents: [DialogComponent, AddCatalogComponent, AddUserComponent, DeleteCatalogueComponent, DeleteUserComponent, EditCatalogComponent, EditUserComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MatTabsModule,
		MatCardModule,
		DragDropModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatGridListModule,
		MatMenuModule,
		MatTableModule,
		MatPaginatorModule,
		MatSortModule,
		FlexLayoutModule,
		MatFormFieldModule,
		MatInputModule,
		MatChipsModule,
    MatDialogModule,
    SweetAlert2Module,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
    MatSelectModule,
    MatCheckboxModule,
    DragulaModule.forRoot(),
    HttpClientModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBSXOtpdnSWu6lPGjHHWEwPF9yU4VPuqhA'
    })
	],
    providers: [Globals
    ],
  	bootstrap: [AppComponent]
})
export class AppModule { }
